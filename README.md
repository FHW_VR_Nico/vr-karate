# Kyokushin Karate Tracker

This is a master VR project at the [FH Wedel](https://www.fh-wedel.de/) by Erik Jenning and Nicolas Hollmann, supervised by Marcus Riemer.

The goal of this project is to develop a VR application that supports independent martial arts training in [Kyokushin Karate](https://de.wikipedia.org/wiki/Kyokushin_Kaikan). For this purpose, the movements of a user are to be recorded and evaluated with the help of position trackers. The user should receive feedback on the quality of his execution after performing a fighting technique (punch or block). In this way, independent error correction is to be improved during self-study, thus preventing the consolidation of incorrect techniques.

To know which movements are the correct ones for a technique, the movements of two karate trainers were recorded as references. Those two trainers are Daniel Wähling and Mehdi Ebadi from the [Tekken Dojo](https://www.tekken-dojo.com/) in Wedel.

To check if this application is really able to give valuable feedback to the user, an experiment was done to validate the output.

## Requirements

To run the binary, a Windows PC, a VR Headset using SteamVR and exactly four HTC Vive Tracker are required.
Currently the project was only tested using **Windows 10** using **SteamVR 1.25.7**, a **HTC Vive Pre** and _four_ **HTC Vive Tracker 3.0**.

## Developing

To extend or modify this project, you need to setup a development environment. This application was developed with Unity 2021.3.10f1. If possible, you should use exactly this version to avoid compatibility issues. The repository itself is already the Unity Project, you can open it directly.

**The technique videos were removed for legal reasons.**

## Assets

**Akashi** Font https://www.fontsquirrel.com/fonts/akashi

**Banana Man** 3D Model https://assetstore.unity.com/packages/3d/characters/humanoids/banana-man-196830

**FH Wedel Logo** https://www.fh-wedel.de/

**Tekken Dojo Logo** supplied by the Dojo


