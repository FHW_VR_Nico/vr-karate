MERGED RECORDINGS README [ENGLISH]
===========================

This folder contains the combined recordings from the trainers that were created with the "Recorded Data Window".
These are the average movements of all techniques, from which the TechSequence was then generated.
The originals are kept here only as a backup and for traceability, but they are not used further in the project.

MERGED RECORDINGS README [ENGLISH]
===========================

In diesem Ordner befinden sich die vereinigten Aufnahmen von den Trainern, die mit dem "Recorded Data Window" erzeugt wurden.
Hierbei handelt es sich um die Durchschnittsbewegungen aller Techniken, aus denen dann die TechSequence erzeugt wurde.
Die Originale werden hier nur als Backup und für die Nachvollziehbarkeit aufbewart, sie werden aber nicht weiter im Projekt verwendet.
