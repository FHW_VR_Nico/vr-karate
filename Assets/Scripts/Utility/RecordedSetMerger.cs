using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Utility class to merge a list of RecordedSets.
/// </summary>
public class RecordedSetMerger
{
    private const float MERGE_TIMESTEP = 0.0111f;

    private List<RecordedSet> mergeSets;
    private float fullTime;
    private float setsWeight;

    private List<float> timings = new List<float>();

    /// <summary>
    /// Create a new merger with the given sets.
    /// </summary>
    /// <param name="sets">sets to merge</param>
    public RecordedSetMerger(List<RecordedSet> sets)
    {
        if (sets.Count < 2)
        {
            Debug.LogError("Merging recorded sets requires at least 2 sets!");
            sets = null;
        }
        mergeSets = sets;
        InitSets();
        InitTimings();
    }

    /// <summary>
    /// Normalize all sets and calculate minimal full time.
    /// </summary>
    private void InitSets()
    {
        setsWeight = 1.0f / mergeSets.Count;

        fullTime = float.PositiveInfinity;
        foreach (RecordedSet mSet in mergeSets)
        {
            mSet.Normalize();
            if (mSet.Time < fullTime)
            {
                fullTime = mSet.Time;
            }
        }
    }

    /// <summary>
    /// Create a new timing list with the given MERGE_TIMESTEP over the full time.
    /// </summary>
    private void InitTimings()
    {
        for (float globalT = 0.0f; globalT <= fullTime; globalT += MERGE_TIMESTEP)
        {
            timings.Add(globalT);
        }
    }

    /// <summary>
    /// Perform the merging.
    /// </summary>
    /// <returns>merged set or null on error</returns>
    public RecordedSet MergeSets()
    {
        if (mergeSets == null)
        {
            Debug.LogError("Can't merge sets!");
            return null;
        }

        RecordedSet resultSet = new RecordedSet(timings, fullTime);

        // This is safe, we checked in the constructor that we have at least two sets.
        string[] inputSources = mergeSets[0].InputSources;
        foreach (string inputSource in inputSources)
        {
            resultSet.AddData(MergeRecordedData(inputSource));
        }

        return resultSet;
    }

    /// <summary>
    /// Merge the RecordedData of one input source.
    /// </summary>
    /// <param name="inputSource">the input source to merge</param>
    /// <returns>merged RecordedData</returns>
    private RecordedData MergeRecordedData(string inputSource)
    {
        // Initialize required lists for merging.
        RecordedData[] inputData = new RecordedData[mergeSets.Count];
        int[] indices = new int[mergeSets.Count];
        List<Vector3> recordedPositions = new List<Vector3>(timings.Count);
        List<Quaternion> recordedOrientations = new List<Quaternion>(timings.Count);

        // Get the RecordedData of all sets
        for (int setId = 0; setId < mergeSets.Count; setId++)
        {
            mergeSets[setId].GetData(inputSource, out inputData[setId]);
            if (inputData[setId] == null)
            {
                Debug.LogError("At least one set doesn't have the input source " + inputSource);
                return null;
            }
        }

        // Iterate over all timings and calculate a new merged entry
        foreach (float globalT in timings)
        {
            Vector3 currPos = Vector3.zero;
            Quaternion currRot = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

            // Calculate new average position and orientation out of all sets for the given time index
            for (int setId = 0; setId < mergeSets.Count; setId++)
            {
                // Find the right indices for the current set and current time index
                while (globalT > mergeSets[setId].timings[indices[setId] + 1])
                {
                    indices[setId]++;
                }

                // Find the two entries in the current set that are nearest to the global time and get their ratio
                int timeIdx = indices[setId];
                float currentTime = globalT - mergeSets[setId].timings[timeIdx];
                float totalTime = mergeSets[setId].timings[timeIdx + 1] - mergeSets[setId].timings[timeIdx];
                float smallT = currentTime / totalTime;

                // Add the interpolated position to the average position
                // The average comes from the setsWeight 
                currPos += setsWeight * Vector3.Lerp(
                    inputData[setId].recordedPositions[timeIdx], 
                    inputData[setId].recordedPositions[timeIdx + 1], smallT);

                // Quaternions are a lot harder to average.
                // First we find the interpolated quaternion for the local time
                Quaternion newRot = Quaternion.Lerp(
                    inputData[setId].recordedOrientations[timeIdx],
                    inputData[setId].recordedOrientations[timeIdx + 1], smallT);

                // Perform the weighted adding of the quaternion to get an average at the end
                float quatWeight = 1.0f;
                if (Quaternion.Dot(newRot, currRot) < 0.0f)
                {
                    quatWeight = -1.0f;
                }

                currRot.Set(
                    currRot.x + quatWeight * newRot.x,
                    currRot.y + quatWeight * newRot.y,
                    currRot.z + quatWeight * newRot.z,
                    currRot.w + quatWeight * newRot.w
                );
            }

            // We need to normalize the quaternion after the manual additions
            currRot.Normalize();

            // Add the calculated position and orientation to the result list
            recordedPositions.Add(currPos);
            recordedOrientations.Add(currRot);
        }

        return new RecordedData(inputSource, recordedPositions, recordedOrientations, Vector3.zero, Quaternion.identity, 0.0f);
    }
}
