using System;
using UnityEngine;

/// <summary>
/// Utility class to calculate a score out of a reference set and a current set.
/// </summary>
public class ScoreCalculator 
{ 
    private const string INPUT_SOURCE_LEFT_HAND   = "LeftHand";
    private const string INPUT_SOURCE_RIGHT_HAND  = "RightHand";
    private const string INPUT_SOURCE_LEFT_ELBOW  = "LeftElbow";
    private const string INPUT_SOURCE_RIGHT_ELBOW = "RightElbow";
    private const string INPUT_SOURCE_HEAD        = "Head";


    private TechSequence.GlobalWeights globalWeights;
    private ITimeSeriesComparator<Vector3> algorithmVec3;
    private ITimeSeriesComparator<Quaternion> algorithmQuat;

    /// <summary>
    /// Creates a new ScoreCalculator.
    /// </summary>
    /// <param name="globalWeights">the global weights</param>
    /// <param name="algorithmVec3">algorithm to compare vector time series</param>
    /// <param name="algorithmQuat">algorithm to compare quaternion time series</param>
    public ScoreCalculator(
        TechSequence.GlobalWeights globalWeights, 
        ITimeSeriesComparator<Vector3> algorithmVec3, 
        ITimeSeriesComparator<Quaternion> algorithmQuat)
    {
        this.globalWeights = globalWeights;
        this.algorithmVec3 = algorithmVec3;
        this.algorithmQuat = algorithmQuat;
    }

    /// <summary>
    /// Compares two normalized RecordedSets and returns a score of how similar they are.
    /// 5 is best, 1 is worst.
    /// </summary>
    /// <param name="weights">the weights for the scoring</param>
    /// <param name="referenceSet">the normalized reference data</param>
    /// <param name="currentSet">the normalized current data</param>
    /// <returns>returns a score between 1 and 5 (inclusive)</returns>
    public int Calculate(TechSequence.EntryWeights weights, RecordedSet referenceSet, RecordedSet currentSet)
    {
        float dataCount = (float)(referenceSet.Count);

        string[] inputSources = currentSet.InputSources;

        float vecErrorSum = 0.0f;
        float quatErrorSum = 0.0f;
        float weightSum = 0.0f;

        for (int i = 0; i < inputSources.Length; i++)
        {
            RecordedData referenceData;
            RecordedData currentData;
            currentSet.GetData(inputSources[i], out currentData);
            if (!referenceSet.GetData(inputSources[i], out referenceData))
            {
                Debug.LogWarning(inputSources[i] + " missing in dataset!");
                continue;
            }

            // Compare positions
            float resultVec = algorithmVec3.CalcDistance(currentData.recordedPositions, referenceData.recordedPositions);
            resultVec /= dataCount;

            // Compare orientations
            float resultQuat = algorithmQuat.CalcDistance(currentData.recordedOrientations, referenceData.recordedOrientations);
            resultQuat /= dataCount;

            // Weight by technique
            resultVec = WeightVecError(resultVec, weights, inputSources[i]);
            resultQuat = WeightQuatError(resultQuat, weights, inputSources[i]);

            // Weight by inputSource
            float resultWeight = WeightByInputSource(inputSources[i]);

            weightSum += resultWeight;
            vecErrorSum += resultVec * resultWeight;
            quatErrorSum += resultQuat * resultWeight;
        }

        vecErrorSum /= weightSum;
        quatErrorSum /= weightSum;

        float combinedError =   globalWeights.vecQuatRatio          * vecErrorSum
                              + (1.0f - globalWeights.vecQuatRatio) * quatErrorSum;
        int starsLost = (int)Math.Round(4 * combinedError);

        return 5 - starsLost; 
    }

    /// <summary>
    /// Weights given vec3-error based on technique and input-source.
    /// Afterwards maps result to range [0..1].
    /// </summary>
    /// <param name="error">Vec3 error</param>
    /// <param name="weights">the weights for the scoring</param>
    /// <param name="inputSource">Input-source</param>
    /// <returns>Weighted vec3-error mapped to range [0..1]</returns>
    private float WeightVecError(float error, TechSequence.EntryWeights weights, string inputSource) 
    {
        switch (inputSource)
        {
            case INPUT_SOURCE_LEFT_HAND:
            case INPUT_SOURCE_RIGHT_HAND:
                return weights.vecErrorHand.Map(error);

            case INPUT_SOURCE_LEFT_ELBOW:
            case INPUT_SOURCE_RIGHT_ELBOW:
                return weights.vecErrorElbow.Map(error);

            case INPUT_SOURCE_HEAD:
                return weights.vecErrorHead.Map(error);

            default:
                Debug.LogError("No weight defined for input-source " + inputSource + ", data will be ignored!");
                return -1.0f;
        }
    }

    /// <summary>
    /// Weights given quat-error based on technique and input-source.
    /// Afterwards maps result to range [0..1].
    /// </summary>
    /// <param name="error">Quat error</param>
    /// <param name="weights">the weights for the scoring</param>
    /// <param name="inputSource">Input-source</param>
    /// <returns>Weighted quat-error mapped to range [0..1]</returns>
    private float WeightQuatError(float error, TechSequence.EntryWeights weights, string inputSource)
    {
        switch (inputSource)
        {
            case INPUT_SOURCE_LEFT_HAND:
            case INPUT_SOURCE_RIGHT_HAND:
                return weights.quatErrorHand.Map(error);

            case INPUT_SOURCE_LEFT_ELBOW:
            case INPUT_SOURCE_RIGHT_ELBOW:
                return weights.quatErrorElbow.Map(error);

            case INPUT_SOURCE_HEAD:
                return weights.quatErrorHead.Map(error);

            default:
                Debug.LogError("No weight defined for input-source " + inputSource + ", data will be ignored!");
                return -1.0f;
        }
    }

    /// <summary>
    /// Returns weight for a given input source.
    /// </summary>
    /// <param name="inputSource">Input-source</param>
    /// <returns>Weight for the given input source, may be greater than 1.0f</returns>
    private float WeightByInputSource(string inputSource)
    {
        switch (inputSource)
        {
            case INPUT_SOURCE_LEFT_HAND:
            case INPUT_SOURCE_RIGHT_HAND:
                return globalWeights.hand;

            case INPUT_SOURCE_LEFT_ELBOW:
            case INPUT_SOURCE_RIGHT_ELBOW:
                return globalWeights.elbow;

            case INPUT_SOURCE_HEAD:
                return globalWeights.head;

            default:
                // This code should not be reachable, if the TechSequence includes all input sources.
                Debug.LogError("No top-level weight defined for input-source " + inputSource + ", data will be ignored!");
                return 0.0f;
        }
    }
}
