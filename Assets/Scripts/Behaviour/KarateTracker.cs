using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// This behaviour controlls the game loop in the training room scene.
/// It manages all interactions and states.
/// </summary>
[AddComponentMenu("Karate/Karate Tracker")]
public class KarateTracker : MonoBehaviour
{
    private const string ERROR_NO_CALIBRATION = "Please calibrate first";

    /// <summary>
    /// Possible states for the game
    /// </summary>
    public enum State
    {
        Menu,
        Calibration,
        Sequence,
        Tracking,
    };

    public List<Transform> trackedObjs;
    public TechSequence techSequence;

    [Header("UI")]
    public TextMeshProUGUI  countdownLabel;
    public GameObject       textCanvas;
    public GameObject       videoCanvas;
    public GameObject       scoreCanvas;
    public GameObject       mainMenu;
    public GameObject       sequenceMenu;
    public Color            starSet;
    public Color            starUnset;

    private TextMeshProUGUI lastTechLabel;
    private TextMeshProUGUI textCanvasLabel;
    private Animator        textCanvasAnimator;
    private Animator        startTriggerAnimator;
    private Animator        calibrationTriggerAnimator;
    private Animator[]      starAnimators = new Animator[5];
    private Image[]         scoreStars = new Image[5];
    private VideoPlayer     techniqueVideoPlayer;

    [Header("Subcontroller")]
    public CalibrationController calibrationController;
    public LineController lineController;
    public TrackingController trackingController;

    private State state = State.Menu;
    private int techIdx = 0;
    private ScoreCalculator scoreCalculator;

    // These two variables are volatile because they are accessed by two threads
    private volatile int calculatedScore = 1;
    private volatile bool calculatedScoreReady = false;

    void OnEnable()
    {
        // Init score calculator
        scoreCalculator = new ScoreCalculator(
            techSequence.globalWeights,
            new DTW<Vector3>(Vector3.Distance), 
            new DTW<Quaternion>(Quaternion.Angle)
        );

        // Init training UI
        countdownLabel.enabled = false;
        textCanvas.SetActive(false);
        textCanvasLabel = textCanvas.GetComponentInChildren<TextMeshProUGUI>();
        textCanvasAnimator = textCanvas.GetComponent<Animator>();
        sequenceMenu.SetActive(false);

        // Init score board
        lastTechLabel           = scoreCanvas.transform.Find("Panel/TechniqueLabel").GetComponent<TextMeshProUGUI>();
        Transform starContainer = scoreCanvas.transform.Find("Panel/Stars");
        for (int i = 0; i < scoreStars.Length; i++)
        {
            Transform starObject = starContainer.GetChild(i);

            scoreStars[i] = starObject.GetComponent<Image>();
            starAnimators[i] = starObject.GetComponent<Animator>();
        }
        scoreCanvas.SetActive(false);

        // Init videoplayer
        techniqueVideoPlayer    = videoCanvas.transform.Find("Panel/VideoPlayer").GetComponent<VideoPlayer>();
        ShowVideoPlayer(null);

        // Init main menu animations
        startTriggerAnimator        = mainMenu.transform.Find("StartTrigger").GetComponent<Animator>();
        calibrationTriggerAnimator  = mainMenu.transform.Find("CalibrationTrigger").GetComponent<Animator>();
        calibrationTriggerAnimator.SetBool("HighlightChargeButton", true);

        // Init all controllers
        calibrationController.Init(trackedObjs[0], countdownLabel, textCanvas);
        lineController.Init(this.transform, trackedObjs.Count);
        trackingController.Init(trackedObjs, countdownLabel); 
    }

    void Update()
    {
        // Update is depending on the current game state
        switch (state)
        {
            case State.Calibration:
                if (calibrationController.Update())
                {
                    mainMenu.SetActive(true);
                    trackingController.armLength = calibrationController.ArmLength;
                    state = State.Menu;
                }
                break;
            case State.Sequence:
                // Wait for the other thread to complete calculation
                if (calculatedScoreReady)
                {
                    calculatedScoreReady = false;
                    ShowScoreboard(techSequence.techniques[techIdx].label, calculatedScore);
                    sequenceMenu.SetActive(true);
                    textCanvas.SetActive(true);
                    NextTechnique();
                }
                break;
            case State.Tracking:
                if (trackingController.Update())
                    EndTracking();
                break;
        }
    }

    /// <summary>
    /// Exit the game or stop play mode.
    /// This method can be called as an event handler.
    /// </summary>
    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }

    /// <summary>
    /// Start the calibration mode.
    /// Can only be called in menu state.
    /// This method can be called as an event handler.
    /// </summary>
    public void StartCalibrate()
    {
        if (state != State.Menu)
        {
            Debug.LogError("Tried to start calibration while not in menu state!");
            return;
        }

        calibrationTriggerAnimator.SetBool("HighlightChargeButton", false);

        state = State.Calibration;
        mainMenu.SetActive(false);
        lineController.ClearPointLines();
        calibrationController.StartCalibrate();
    }

    /// <summary>
    /// Start the sequence mode, i.e. the mode where the player can choose and start a technique.
    /// Can only be called in menu state.
    /// This method can be called as an event handler.
    /// </summary>
    public void StartSequence()
    {
        if (state != State.Menu)
        {
            Debug.LogError("Tried to start sequence while not in menu state!");
            return;
        }

        textCanvas.SetActive(true);


        // Check if calibrated, if not show error
        if (trackingController.armLength < float.Epsilon)
        {
            startTriggerAnimator.SetTrigger("ChargeButtonBlocked");
            textCanvasAnimator.SetTrigger("CanvasAlert");

            textCanvasLabel.text = ERROR_NO_CALIBRATION;
            Debug.LogError("Tried to start tracking while not calibrated!");
            return;
        }

        state = State.Sequence;
        mainMenu.SetActive(false);
        sequenceMenu.SetActive(true);
        techIdx = -1;
        NextTechnique();
    }

    /// <summary>
    /// Stop the sequence mode, i.e. return to the main menu.
    /// Can only be called in sequence state.
    /// This method can be called as an event handler.
    /// </summary>
    public void StopSequence()
    {
        if (state != State.Sequence)
        {
            Debug.LogError("Tried to stop sequence while not in sequence state!");
            return;
        }

        state = State.Menu;
        mainMenu.SetActive(true);
        sequenceMenu.SetActive(false);
        textCanvas.SetActive(false);
        scoreCanvas.SetActive(false);
        ShowVideoPlayer(null);
        lineController.ClearPointLines();
    }

    /// <summary>
    /// Start the tracking mode
    /// Can only be called in sequence state.
    /// This method can be called as an event handler.
    /// </summary>
    public void StartTracking()
    {
        if (state != State.Sequence)
        {
            Debug.LogError("Tried to start tracking while not in sequence state!");
            return;
        }

        state = State.Tracking;
        sequenceMenu.SetActive(false);
        textCanvas.SetActive(false);
        scoreCanvas.SetActive(false);
        ShowVideoPlayer(null);
        lineController.ClearPointLines();
        trackingController.StartTracking();
    }

    /// <summary>
    /// Stops the tracking mode and returns to sequence state.
    /// Can only be called in tracking state.
    /// </summary>
    private void EndTracking()
    {
        if (state != State.Tracking)
        {
            Debug.LogError("Tried to end tracking while not in tracking state!");
            return;
        }

        RecordedSet recordedSet = trackingController.LastRecordedSet;

        DrawTrackedLines(recordedSet);
        SaveTrackings(recordedSet);

        // Start score calculation
        recordedSet.Normalize();
        CalculateScoreThread(
            techSequence.techniques[techIdx].weights, 
            techSequence.techniques[techIdx].recordedSet,
            recordedSet);

        state = State.Sequence;
    }

    /// <summary>
    /// Show the movement lines for all trackers.
    /// </summary>
    /// <param name="recordedSet">the recorded movements of all trackers</param>
    private void DrawTrackedLines(RecordedSet recordedSet)
    {
        for (int i = 0; i < trackedObjs.Count; i++)
        {
            // Check if the object or a parent has a behaviour pose where we can read the input source
            SteamVR_Behaviour_Pose vrPose = trackedObjs[i].GetComponentInParent<SteamVR_Behaviour_Pose>();
            if (vrPose == null)
                continue;

            string inputSource = vrPose.inputSource.ToString();

            // Extract recorded data from set
            RecordedData data;
            if (!recordedSet.GetData(inputSource, out data))
            {
                Debug.LogError("Input source " + inputSource + " not in record set.");
                continue;
            }

            // Show the line
            lineController.DrawPointLine(data.PositionsWithStart(), i);
        }
    }

    /// <summary>
    /// Save a tracking to the persistent data path.
    /// This may be useful for later adjustments of the scoring weights.
    /// </summary>
    /// <param name="recordedSet">the recorded set to save</param>
    private void SaveTrackings(RecordedSet recordedSet)
    {
        string filename = Application.persistentDataPath + "/trackings/";
        filename += DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff_") + techSequence.techniques[techIdx].technique.ToString();
        
        recordedSet.ToJsonFile(filename + ".json");

        Debug.Log("Saved trackings to file.");
    }

    /// <summary>
    /// Select the previous technique.
    /// Can only be called in sequence state.
    /// This method can be called as an event handler.
    /// </summary>
    public void PreviousTechnique()
    {
        if (state != State.Sequence)
        {
            Debug.LogError("Tried to load previous technique while not in sequence state!");
            return;
        }

        techIdx = techIdx != 0 ? techIdx-1 : techSequence.techniques.Count-1;
        textCanvasLabel.text = techSequence.techniques[techIdx].label;
        ShowVideoPlayer(techSequence.techniques[techIdx].video);
    }

    /// <summary>
    /// Select the next technique.
    /// Can only be called in sequence state.
    /// This method can be called as an event handler.
    /// </summary>
    public void NextTechnique() 
    {
        if (state != State.Sequence)
        {
            Debug.LogError("Tried to load next technique while not in sequence state!");
            return;
        }

        techIdx = (techIdx + 1) % techSequence.techniques.Count;
        textCanvasLabel.text = techSequence.techniques[techIdx].label;
        ShowVideoPlayer(techSequence.techniques[techIdx].video);
    }

    /// <summary>
    /// Starts a new thread for the score calculation.
    /// A thread is required to avoid a noticible lag in the game which 
    /// can be disorientating for the player.
    /// </summary>
    /// <param name="weights">weights for the technique</param>
    /// <param name="referenceSet">prerecorded reference movements</param>
    /// <param name="currentSet">newly recorded movements</param>
    private void CalculateScoreThread(TechSequence.EntryWeights weights, RecordedSet referenceSet, RecordedSet currentSet)
    {
        calculatedScoreReady = false;
        Thread scoreThread = new Thread(() => {
            calculatedScore = scoreCalculator.Calculate(weights, referenceSet, currentSet);
            calculatedScoreReady = true;
        });

        // Fire and forget the thread.
        // It will notice the main thread by setting calculatedScoreReady to true.
        scoreThread.Start();
    }

    /// <summary>
    /// Shows the score for the last technique in the UI.
    /// Can only be called in sequence state.
    /// </summary>
    /// <param name="technique">the readable name of the last technique</param>
    /// <param name="stars">the score between 1 and 5 (inclusive)</param>
    private void ShowScoreboard(string technique, int stars)
    {
        if (state != State.Sequence)
        {
            Debug.LogError("Tried to show scoreboard while not in sequence state!");
            return;
        }

        lastTechLabel.text = technique;

        // Show canvas
        // The canvas must be active for "SetTrigger" to work!
        scoreCanvas.SetActive(true);
        for (int i = 0; i < scoreStars.Length; i++)
        {
            if (i < stars)
            {
                // Choose right color and start animation
                scoreStars[i].color = starSet;
                starAnimators[i].SetTrigger("StarHighlight");
            } else
            {
                scoreStars[i].color = starUnset;
            }
        }
    }

    /// <summary>
    /// Shows a video clip in the video player.
    /// Can only be called in sequence state.
    /// </summary>
    /// <param name="clip">the clip to show, null to disable the player</param>
    private void ShowVideoPlayer(VideoClip clip)
    {
        if (clip == null)
        {
            techniqueVideoPlayer.Stop();
            videoCanvas.SetActive(false);
            return;
        }

        if (state != State.Sequence)
        {
            Debug.LogError("Tried to show video while not in sequence state!");
            return;
        }

        techniqueVideoPlayer.clip = clip;
        videoCanvas.SetActive(true);
    }
}
