using System.Collections.Generic;
using System;
using UnityEngine;
using Valve.VR;
using TMPro;

/// <summary>
/// This behaviour controlls the game loop in the recording room scene.
/// It manages all interactions and states.
/// </summary>
[AddComponentMenu("Karate/Recording Tracker")]
public class RecordingTracker : MonoBehaviour
{
    /// <summary>
    /// Possible states for the recording room
    /// </summary>
    public enum State
    {
        Menu,
        Calibration,
        Tracking,
    };

    public List<Transform> trackedObjs;

    [Header("Recording")]
    public string person;
    public KarateTechnique technique;
    public uint hitNum;

    [Header("UI")]
    public TextMeshProUGUI personLabel;
    public TextMeshProUGUI techniqueLabel;
    public TextMeshProUGUI hitLabel;
    public TextMeshProUGUI countdownLabel;
    public GameObject      textCanvas;
    public GameObject      mainMenu;

    [Header("Subcontroller")]
    public CalibrationController calibrationController;
    public LineController lineController;
    public TrackingController trackingController;

    private State state = State.Menu;

    /// <summary>
    /// Getter for the current state.
    /// Is required for the editor gui to work.
    /// </summary>
    public State CurrentState { get { return state; } }

    void OnEnable()
    {
        countdownLabel.enabled = false;
        textCanvas.SetActive(false);

        // Init recording settings
        hitNum = 0;
        person = "";
        technique = KarateTechnique.TrackerTest;

        // Init all controllers
        calibrationController.Init(trackedObjs[0], countdownLabel, textCanvas);
        lineController.Init(this.transform, trackedObjs.Count);
        trackingController.Init(trackedObjs, countdownLabel);

        UpdateRecordingCanvas();
    }

    void Update()
    {
        // Update is depending on the current game state
        switch (state) {
            case State.Calibration:
                if (calibrationController.Update())
                {
                    mainMenu.SetActive(true);
                    trackingController.armLength = calibrationController.ArmLength;
                    state = State.Menu;
                }
                break;
            case State.Tracking:
                if (trackingController.Update())
                    EndTracking();

                break;
        }

        UpdateRecordingCanvas();
    }

    /// <summary>
    /// Update the recording display to show the person, technque and hit number.
    /// It is not very efficent to do this every frame, but this is the simplest way
    /// to react to a change initiated in the inspector.
    /// All settings may changed at runtime at any point in time and we want to reflect the change
    /// as fast as possible.
    /// As this whole class is only used in the Editor for recording the
    /// references it is ok to be little bit sloppy.
    /// </summary>
    private void UpdateRecordingCanvas()
    {
        personLabel.SetText(person == "" ? "??" : person);
        techniqueLabel.SetText(technique.ToString());
        hitLabel.SetText("Hit: " + hitNum.ToString());
    }

    /// <summary>
    /// Start the calibration mode.
    /// Can only be called in menu state.
    /// This method can be called as an event handler.
    /// </summary>
    public void StartCalibrate()
    {
        if (state != State.Menu)
        {
            Debug.LogError("Tried to start calibration while not in menu state!");
            return;
        }

        state = State.Calibration;
        mainMenu.SetActive(false);
        lineController.ClearPointLines();
        calibrationController.StartCalibrate();
    }

    /// <summary>
    /// Start the tracking mode
    /// Can only be called in menu state and after calibration.
    /// This method can be called by the editor GUI.
    /// </summary>
    public void StartTracking()
    {
        if (state != State.Menu)
        {
            Debug.LogError("Tried to start tracking while not in menu state!");
            return;
        }
        if (trackingController.armLength < float.Epsilon)
        {
            Debug.LogError("Tried to start tracking while not calibrated!");
            return;
        }

        state = State.Tracking;
        mainMenu.SetActive(false);
        lineController.ClearPointLines();
        trackingController.StartTracking();
    }

    /// <summary>
    /// Stops the tracking mode and returns to menu state.
    /// Can only be called in tracking state.
    /// </summary>
    private void EndTracking()
    {
        if (state != State.Tracking)
        {
            Debug.LogError("Tried to end tracking while not in tracking state!");
            return;
        }

        state = State.Menu;
        RecordedSet recordedSet = trackingController.LastRecordedSet;

        DrawTrackedLines(recordedSet);

        SaveRecordings(recordedSet);
        mainMenu.SetActive(true);
    }

    /// <summary>
    /// Show the movement lines for all trackers.
    /// </summary>
    /// <param name="recordedSet">the recorded movements of all trackers</param>
    private void DrawTrackedLines(RecordedSet recordedSet) 
    {
        for (int i = 0; i < trackedObjs.Count; i++)
        {
            // Check if the object or a parent has a behaviour pose where we can read the input source
            SteamVR_Behaviour_Pose vrPose = trackedObjs[i].GetComponentInParent<SteamVR_Behaviour_Pose>();
            if (vrPose == null)
                continue;

            string inputSource = vrPose.inputSource.ToString();

            // Extract recorded data from set
            RecordedData data;
            if (!recordedSet.GetData(inputSource, out data))
            {
                Debug.LogError("Input source " + inputSource + " not in record set.");
                continue;
            }

            // Show the line
            lineController.DrawPointLine(data.PositionsWithStart(), i);
        }
    }

    /// <summary>
    /// Save a recording to the persistent data path.
    /// If a person name is set, it will be saved with the name, technqiue and hit number.
    /// Otherwise only a timestamp is used as the filename.
    /// </summary>
    /// <param name="recordedSet">the recorded set to save</param>
    private void SaveRecordings(RecordedSet recordedSet) 
    {
        string filename = Application.persistentDataPath + "/recordings/";
        if (person != null && person.Length > 0)
        {
            filename += person + "_" + technique.ToString() + "_" + hitNum.ToString("00");
            hitNum++;
        }
        else
        {
            filename += DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff");
        }
        recordedSet.ToJsonFile(filename + ".json");

        Debug.Log("Saved recordings to file.");
    }
}
