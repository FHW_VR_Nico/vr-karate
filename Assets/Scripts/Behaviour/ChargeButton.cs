using UnityEngine;
using UnityEngine.Events;
using System;

/// <summary>
/// Event that is emitted when the charge button is triggered.
/// </summary>
[Serializable]
public class ChargeButtonEvent : UnityEvent { }

/// <summary>
/// A simple charge button, i.e. a button that is triggered by holding a hand into it for a specific time.
/// </summary>
[AddComponentMenu("Karate/Charge Button")]
public class ChargeButton : MonoBehaviour
{
    public float chargeTime = 3.0f;
    public RectTransform loadingbar;
    public ChargeButtonEvent OnChargeButton;

    private float currentTime = 0.0f;
    private bool eventFired = false;
    private int colliderCount = 0;

    private void OnEnable()
    {
        colliderCount = 0;
        currentTime = chargeTime;
        loadingbar.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Keep track of how many objects are in the trigger
            colliderCount++;

            // Start charging only for the first object
            if (colliderCount == 1)
            {
                currentTime = chargeTime;
                eventFired = false;
                loadingbar.localScale = new Vector3(0, 1, 1);
                loadingbar.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            colliderCount--;
            colliderCount = Math.Max(0, colliderCount);

            // Only stop charging if all objects are removed from the trigger
            if (colliderCount == 0)
            {
                currentTime = chargeTime;
                loadingbar.gameObject.SetActive(false);
            }
        }
    }

    void Update()
    {
        if (colliderCount > 0 && !eventFired)
        {
            // Update time and display it
            currentTime -= Time.deltaTime;
            float percent = (chargeTime - currentTime) / chargeTime;
            loadingbar.localScale = new Vector3(Math.Clamp(percent, 0, 1), 1, 1);

            // Fire event
            if (currentTime <= 0.0f)
            {
                eventFired = true;
                if (OnChargeButton != null)
                {
                    OnChargeButton.Invoke();
                }
            }
        }
    }
}
