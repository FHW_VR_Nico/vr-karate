using System.Collections.Generic;
using System;

using IntPair = Pair<int, int>;

/// <summary>
/// Dynamic time warping algorithm to compare two time series.
/// </summary>
/// <typeparam name="T">type of the data entries</typeparam>
public class DTW<T> : ITimeSeriesComparator<T>
{
    private ITimeSeriesComparator<T>.CalcCost calcCost;

    /// <summary>
    /// Create a new DTW comparator.
    /// </summary>
    /// <param name="calcCost">a function to calculate the cost between two data entries</param>
    public DTW(ITimeSeriesComparator<T>.CalcCost calcCost)
    {
        this.calcCost = calcCost;
    }

    public float CalcDistance(List<T> timeSeriesA, List<T> timeSeriesB)
    {
        return CalcMatrix(timeSeriesA, timeSeriesB)[timeSeriesA.Count - 1, timeSeriesB.Count - 1].Val;
    }

    /// <summary>
    /// Calculates the optimal path in the distance matrix.
    /// </summary>
    /// <param name="timeSeriesA">first time series</param>
    /// <param name="timeSeriesB">second time series</param>
    /// <param name="window">constraint which reduces the search space, null to include full matrix</param>
    /// <returns>optimal path</returns>
    public Pair<List<IntPair>, MatrixVal[,]> CalcPath(List<T> timeSeriesA, List<T> timeSeriesB, List<IntPair> window = null)
    {
        MatrixVal[,] dtwMatrix = CalcMatrix(timeSeriesA, timeSeriesB, window);
        List<IntPair> result = new List<IntPair>((int)(Math.Max(timeSeriesA.Count, timeSeriesB.Count) * 1.5f));

        int i = timeSeriesA.Count - 1;
        int j = timeSeriesB.Count - 1;

        // This works a little bit like Dijkstra.
        while (i != 0 && j != 0)
        {
            result.Add(new IntPair(i, j));
            int newI = dtwMatrix[i, j].PrevI;
            j = dtwMatrix[i, j].PrevJ;
            i = newI;
        }

        result.Reverse();

        return new Pair<List<IntPair>, MatrixVal[,]>(result, dtwMatrix);
    }

    /// <summary>
    /// Computes the DTW-specific distance matrix between each time series.
    /// </summary>
    /// <param name="timeSeriesA">first time series</param>
    /// <param name="timeSeriesB">second time series</param>
    /// <param name="window">constraint which reduces the search space, null to include full matrix</param>
    /// <returns>distance matrix</returns>
    private MatrixVal[,] CalcMatrix(List<T> timeSeriesA, List<T> timeSeriesB, List<IntPair> window = null)
    {
        MatrixVal[,] dtwMatrix = new MatrixVal[timeSeriesA.Count, timeSeriesB.Count];
        int lenA = timeSeriesA.Count;
        int lenB = timeSeriesB.Count;

        // If no window is supplied, we create a window wich encloses the entire matrix
        if (window == null)
        {
            window = new List<IntPair>(lenA * lenB);
            for (int a = 1; a < lenA; a++)
            {
                for (int b = 1; b < lenB; b++)
                {
                    window.Add(new IntPair(a, b));
                }
            }
        }

        // Initialize the matrix with infinity
        for (int i = 0; i < timeSeriesA.Count; i++)
        {
            for (int j = 0; j < timeSeriesB.Count; j++)
            {
                dtwMatrix[i, j] = new MatrixVal(float.PositiveInfinity, i, j);
            }
        }
        dtwMatrix[0, 0] = new MatrixVal(0.0f, 0, 0);

        // Calculate the cumulated distance for each matrix cell
        foreach (var point in window)
        {
            int i = point.Fst;
            int j = point.Snd;

            float cost = calcCost(timeSeriesA[i], timeSeriesB[j]);
            MatrixVal lastMin = MatrixVal.Min(
                new MatrixVal(dtwMatrix[i - 1, j].Val,     i - 1, j),
                new MatrixVal(dtwMatrix[i, j - 1].Val,     i    , j - 1),
                new MatrixVal(dtwMatrix[i - 1, j - 1].Val, i - 1, j - 1)
            );

            lastMin.Add(cost);
            dtwMatrix[i, j] = lastMin;
        }

        return dtwMatrix;
    }

    /// <summary>
    /// One entry of the DTW distance matrix.
    /// </summary>
    public class MatrixVal
    {
        public float Val { get; private set; }
        public int PrevI { get; private set; }
        public int PrevJ { get; private set; }

        /// <summary>
        /// Create a new entry for a distance matrix.
        /// </summary>
        /// <param name="val">actual value</param>
        /// <param name="i">previous i-coordinate</param>
        /// <param name="j">previous j-coordinate</param>
        public MatrixVal(float val, int i, int j)
        {
            this.Val = val;
            this.PrevI = i;
            this.PrevJ = j;
        }

        /// <summary>
        /// Add something to the current value.
        /// </summary>
        /// <param name="x">what to add</param>
        public void Add(float x)
        {
            this.Val += x;
        }

        /// <summary>
        /// Returns the MatrixVal with the lowest actual value.
        /// </summary>
        /// <param name="vals">all values to compare</param>
        /// <returns>minimal MatrixVal</returns>
        public static MatrixVal Min(params MatrixVal[] vals)
        {
            if (vals.Length == 0)
            {
                return null;
            }

            MatrixVal result = vals[0];
            for (int i = 1; i < vals.Length; i++)
            {
                if (vals[i].Val < result.Val)
                {
                    result = vals[i];
                }
            }

            return result;
        }
    }
}
