using System.Collections.Generic;

/// <summary>
/// Interface for a comparison of two time series of data 
/// entries by calculating a abstract distance.
/// </summary>
/// <typeparam name="T">type of the data entries</typeparam>
public interface ITimeSeriesComparator<T>
{
    /// <summary>
    /// Calculates the distance between two data entries.
    /// </summary>
    /// <param name="a">a data entry of the first series</param>
    /// <param name="b">a data entry of the second series</param>
    /// <returns>concrete distance</returns>
    public delegate float CalcCost(T a, T b);

    /// <summary>
    /// Calculate the cumulated distance of two time series.
    /// </summary>
    /// <param name="timeSeriesA">fist time series</param>
    /// <param name="timeSeriesB">second time series</param>
    /// <returns>cumulated distance</returns>
    float CalcDistance(List<T> timeSeriesA, List<T> timeSeriesB);
}
