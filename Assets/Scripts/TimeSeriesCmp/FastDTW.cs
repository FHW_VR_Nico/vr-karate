using System.Collections.Generic;

using IntPair = Pair<int, int>;

/// <summary>
/// Fast dynamic time warping algorithm to compare two time series.
/// </summary>
/// <typeparam name="T">type of the data entries</typeparam>
public class FastDTW<T> : ITimeSeriesComparator<T>
{
    public delegate T CalcAverage(T a, T b);

    private CalcAverage calcAverage;

    private DTW<T> normalDtw;

    private int radius;

    /// <summary>
    /// Create a new FastDTW comparator.
    /// </summary>
    /// <param name="calcCost">a function to calculate the cost between two data entries</param>
    /// <param name="calcAverage">a function to calculate the average between two data entries</param>
    /// <param name="radius">window extension radius</param>
    public FastDTW(ITimeSeriesComparator<T>.CalcCost calcCost, CalcAverage calcAverage, int radius)
    {
        this.normalDtw = new DTW<T>(calcCost);
        this.calcAverage = calcAverage;
        this.radius = radius;
    }

    public float CalcDistance(List<T> timeSeriesA, List<T> timeSeriesB)
    {
        var path = CalcPath(timeSeriesA, timeSeriesB);
        return path.Snd[timeSeriesA.Count - 1, timeSeriesB.Count - 1].Val;
    }

    /// <summary>
    /// Calculates the approximated optimal path in the distance matrix.
    /// </summary>
    /// <param name="timeSeriesA">first time series</param>
    /// <param name="timeSeriesB">second time series</param>
    /// <returns>optimal path</returns>
    public Pair<List<IntPair>, DTW<T>.MatrixVal[,]> CalcPath(List<T> timeSeriesA, List<T> timeSeriesB)
    {
        int minTSsize = radius + 2;

        if (timeSeriesA.Count <= minTSsize || timeSeriesB.Count <= minTSsize)
        {
            return normalDtw.CalcPath(timeSeriesA, timeSeriesB);
        }
        else
        {
            List<T> shrunkA = ReduceByHalf(timeSeriesA);
            List<T> shrunkB = ReduceByHalf(timeSeriesB);

            var lowResPath = CalcPath(shrunkA, shrunkB);

            List<IntPair> window = ExpandedResWindow(lowResPath.Fst, timeSeriesA.Count, timeSeriesB.Count);

            return normalDtw.CalcPath(timeSeriesA, timeSeriesB, window);
        }
    }

    /// <summary>
    /// Reduce a time series by half.
    /// We calculate the average between two neigbours to do so.
    /// </summary>
    /// <param name="timeSeries">time series to reduce</param>
    /// <returns>reduced time series</returns>
    private List<T> ReduceByHalf(List<T> timeSeries)
    {
        List<T> result = new List<T>(timeSeries.Count / 2);

        for (int i = 0; i < timeSeries.Count / 2; i++)
        {
            result.Add(calcAverage(timeSeries[i * 2], timeSeries[i * 2 + 1]));
        }

        return result;
    }

    /// <summary>
    /// Construct a expanded window out of a path.
    /// </summary>
    /// <param name="path">path to expand</param>
    /// <param name="lenA">number of entries in the first time series</param>
    /// <param name="lenB">number of entries in the second time series</param>
    /// <returns>expanded window</returns>
    private List<IntPair> ExpandedResWindow(List<IntPair> path, int lenA, int lenB)
    {
        // Expand path by radius
        HashSet<IntPair> pathSet = new HashSet<IntPair>();
        foreach (var point in path)
        {
            for (int x = -radius; x <= radius; x++)
            {
                for (int y = -radius; y <= radius; y++)
                {
                    pathSet.Add(new IntPair(point.Fst + x, point.Snd + y));
                }
            }
        }

        // Build a windows as a set of coordinates
        HashSet<IntPair> windowSet = new HashSet<IntPair>();
        foreach (var point in pathSet)
        {
            windowSet.Add(new IntPair(point.Fst * 2,     point.Snd * 2));
            windowSet.Add(new IntPair(point.Fst * 2 + 1, point.Snd * 2));
            windowSet.Add(new IntPair(point.Fst * 2,     point.Snd * 2 + 1));
            windowSet.Add(new IntPair(point.Fst * 2 + 1, point.Snd * 2 + 1));
        }

        // Create a real, sorted window out of the set
        // Inspired by https://github.com/slaypni/fastdtw/blob/ea13ae2e6761f056623ff1d3ac6c71fcf4f94497/fastdtw/fastdtw.py#L171
        List<IntPair>  window = new List<IntPair>(lenA * lenB);
        for (int a = 1; a < lenA; a++)
        {
            for (int b = 1; b < lenB; b++)
            {
                var newPoint = new IntPair(a, b);
                if (windowSet.Contains(newPoint)) 
                {
                    window.Add(newPoint);
                }
            }
        }

        return window;
    }
}
