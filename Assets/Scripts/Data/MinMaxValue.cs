using System;

/// <summary>
/// Container class for a minimum and maximum float value.
/// Similar to a pair but with a fixed type and some helper methods.
/// As it is Serializable it can be used for public members of MonoBehaviours 
/// and similar classes that can be edited in the Inspector.
/// </summary>
[Serializable]
public class MinMaxValue
{
    public float min = 0.0f;
    public float max = 1.0f;

    /// <summary>
    /// The range between min and max
    /// </summary>
    public float Range {
        get
        {
            return max - min;
        }
    }

    /// <summary>
    /// Creates a new MinMaxValue with the given values
    /// </summary>
    /// <param name="min">the min value for the new object</param>
    /// <param name="max">the max value for the new object</param>
    public MinMaxValue(float min, float max)
    {
        this.min = min;
        this.max = max;
    }

    /// <summary>
    /// Maps a value in the range of min and max to a range of 0.0f and 1.0f.
    /// If the value is less than min then 0.0f is always returned.
    /// If the value is greater than max then 1.0f is always returned.
    /// </summary>
    /// <param name="x">the value to map</param>
    /// <returns>a value between 0.0f and 1.0f</returns>
    public float Map(float x)
    {
        return Math.Clamp((x - min) / Range, 0.0f, 1.0f);
    }
}
