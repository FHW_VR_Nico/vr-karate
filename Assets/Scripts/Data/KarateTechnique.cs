using System.ComponentModel;

/// <summary>
/// An enumeration of all supported techniques.
/// All techniques must be performed in Migi Sanchin Dachi.
/// </summary>
public enum KarateTechnique
{
    [Description("Not a real technique, just for testing")]
    TrackerTest,

    [Description("Punch middle, right then left")]
    SeikenZukiChudan,

    [Description("Low punch, right then left")]
    SeikenShitaZuki,

    [Description("Side hook punch, right then left")]
    SeikenKageZuki,

    [Description("Forefist strike to the chin, right then left")]
    SeikenAgoUchi,

    [Description("Front backfist strike, right then left")]
    UrakenShomenGanmenUchi,

    [Description("Forefist double strike middle, twice")]
    MoroteZukiChudan,

    [Description("Middle outside block, right then left")]
    SeikenSotoUke,

    [Description("Upper block, right then left")]
    SeikenJodanUke,

    [Description("Low sweep block, right then left")]
    SeikenGedanBarai,

    [Description("Middle inside block, right then left")]
    SeikenUchiUke
}
