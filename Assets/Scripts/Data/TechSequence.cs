using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

/// <summary>
/// Custom asset type to save a sequence of reference RecordedSets.
/// </summary>
[CreateAssetMenu(fileName = "NewSequence", menuName = "Karate/Technique Sequence", order = 1)]
public class TechSequence : ScriptableObject
{
    /// <summary>
    /// Data class to save all global weights of a TechSequence
    /// </summary>
    [Serializable]
    public class GlobalWeights
    {
        [Range(0.0f, 3.0f)]
        public float hand = 1.0f;

        [Range(0.0f, 3.0f)]
        public float elbow = 1.0f;

        [Range(0.0f, 3.0f)]
        public float head = 1.0f;

        [Range(0.0f, 1.0f)]
        public float vecQuatRatio = 0.5f;
    }

    /// <summary>
    /// Data class to save the weights of single entry in a TechSequence
    /// </summary>
    [Serializable]
    public class EntryWeights
    {
        [Header("Vector")]
        public MinMaxValue vecErrorHand = new MinMaxValue(0.1f, 0.2f);
        public MinMaxValue vecErrorElbow = new MinMaxValue(0.1f, 0.2f);
        public MinMaxValue vecErrorHead = new MinMaxValue(0.1f, 0.15f);

        [Header("Quaternion")]
        public MinMaxValue quatErrorHand = new MinMaxValue(10, 20);
        public MinMaxValue quatErrorElbow = new MinMaxValue(10, 20);
        public MinMaxValue quatErrorHead = new MinMaxValue(10, 20);
    }

    /// <summary>
    /// Data class to save a single entry in a TechSequence
    /// </summary>
    [Serializable]
    public class Entry
    {
        public string label;
        public KarateTechnique technique;
        public VideoClip video;

        public EntryWeights weights;

        /// <summary>
        /// This is the saved recorded set.
        /// We needed to hide it in the inspector as this caused
        /// massive lags in the editor UI if more than three recordings are imported.
        /// As they are imported with a custom inspector from a json file there is
        /// no need to modifiy them here.
        /// </summary>
        [HideInInspector]
        public RecordedSet recordedSet;
    }


    public GlobalWeights globalWeights;

    public List<Entry> techniques = new List<Entry>();
}
