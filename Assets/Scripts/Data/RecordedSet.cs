using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

/// <summary>
/// Data class to save one RecordedData per input source.
/// It provides some helpers to easily access, load and save the contained data.
/// </summary>
[Serializable]
public class RecordedSet
{
    [SerializeField]
    private List<RecordedData> storage = new List<RecordedData>(5);

    public List<float> timings;

    [SerializeField]
    private string datetime;

    [SerializeField]
    private float time;

    /// <summary>
    /// The timestemp when this set was recorded.
    /// </summary>
    public string Datetime {
        get
        {
            return datetime;
        }
    }

    /// <summary>
    /// The length of this set in seconds.
    /// </summary>
    public float Time
    {
        get
        {
            return time;
        }
    }

    /// <summary>
    /// Returns how many data points are saved per input source.
    /// </summary>
    public int Count
    {
        get
        {
            return timings.Count;
        }
    }

    /// <summary>
    /// An array with the names of all input sources in this set.
    /// </summary>
    public string[] InputSources
    {
        get
        {
            string[] sources = new string[storage.Count];
            for (int i = 0; i < storage.Count; i++)
            {
                sources[i] = storage[i].inputSource;
            }
            return sources;
        }
    }

    /// <summary>
    /// Create new empty set.
    /// </summary>
    /// <param name="timings">a list with the timings of this set</param>
    /// <param name="time">length of this set in seconds</param>
    public RecordedSet(List<float> timings, float time)
    {
        this.timings = new List<float>(timings);
        this.datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        this.time = time;
    }

    /// <summary>
    /// Add a new recording for an input source to this set.
    /// </summary>
    /// <param name="recordedData">the new recorded data</param>
    public void AddData(RecordedData recordedData)
    {
        storage.Add(recordedData);
    }

    /// <summary>
    /// Returns the data for a given input source if it exists in this set.
    /// </summary>
    /// <param name="inputSource">input source to get the data from</param>
    /// <param name="recordedData">the returned recorded data, is only used if this method returns true</param>
    /// <returns>true if the input source was found, false if not</returns>
    public bool GetData(string inputSource, out RecordedData recordedData)
    {
        recordedData = null;

        foreach (var elem in storage)
        {
            if (elem.inputSource == inputSource)
            {
                recordedData = elem;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Normalize everything by dividing with the saved normalization value.
    /// </summary>
    public void Normalize()
    {
        foreach (var elem in storage)
        {
            elem.Normalize();
        }
    }

    /// <summary>
    /// Serialize this set to JSON
    /// </summary>
    /// <param name="pretty">pretty print the JSON</param>
    /// <returns>a string containing the JSON</returns>
    public string ToJson(bool pretty = false)
    {
        return JsonUtility.ToJson(this, pretty);
    }

    /// <summary>
    /// Serialize and save this set to a JSON file.
    /// If the target directory does not exists it will be created.
    /// </summary>
    /// <param name="filename">destination filename</param>
    /// <param name="pretty">pretty print JSON</param>
    public void ToJsonFile(string filename, bool pretty = false)
    {
        string dir = Path.GetDirectoryName(filename);
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        File.WriteAllText(filename, this.ToJson(pretty));
    }

    /// <summary>
    /// Deserialize a set from a JSON string.
    /// </summary>
    /// <param name="jsonInput">string to deserialize</param>
    /// <returns>the deserialized set</returns>
    public static RecordedSet FromJson(string jsonInput)
    {
        return JsonUtility.FromJson<RecordedSet>(jsonInput);
    }

    /// <summary>
    /// Deserialize a set from a JSON file.
    /// </summary>
    /// <param name="filename">source filename</param>
    /// <returns>the deserialized set</returns>
    public static RecordedSet FromJsonFile(string filename)
    {
        return FromJson(File.ReadAllText(filename));
    }
}
