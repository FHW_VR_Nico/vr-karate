using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

/// <summary>
/// Data class to save the movements of a single input source.
/// It provides some helpers to easily access, load and save the contained data.
/// </summary>
[Serializable]
public class RecordedData
{
    public string inputSource;

    public List<Vector3>    recordedPositions;
    public List<Quaternion> recordedOrientations;

    [SerializeField]
    private Vector3         startPos;

    [SerializeField]
    private Quaternion      startOrient; // Currently unused, maybe helpful later

    [SerializeField]
    private float           normalization;

    /// <summary>
    /// Create a new RecordedData from recorded movements.
    /// </summary>
    /// <param name="inputSource">name of the input source</param>
    /// <param name="positions">recorded positions over time</param>
    /// <param name="orientations">recorded orientations over time</param>
    /// <param name="startPos">start positions</param>
    /// <param name="startOrient">start orientation</param>
    /// <param name="normalization">normalization value</param>
    public RecordedData(string inputSource, List<Vector3> positions, List<Quaternion> orientations, Vector3 startPos = default(Vector3),
                        Quaternion startOrient = default(Quaternion), float normalization = 1.0f)
    {
        this.inputSource    = inputSource;
        this.startPos       = startPos;
        this.startOrient    = startOrient;
        this.normalization  = normalization;
        this.recordedPositions = new List<Vector3>(positions);
        this.recordedOrientations = new List<Quaternion>(orientations);
    }

    /// <summary>
    /// Return the recorded positions over time but add 
    /// the start position to restore world coordinates.
    /// </summary>
    /// <returns>original positions</returns>
    public List<Vector3> PositionsWithStart()
    {
        List<Vector3> result = new List<Vector3>(recordedPositions);

        for (int i = 0; i < recordedPositions.Count; i++)
        {
            result[i] += startPos;
        }

        return result;
    }

    /// <summary>
    /// Split the positions over time in their corresponding axes.
    /// </summary>
    /// <param name="xs">all x values</param>
    /// <param name="ys">all y values</param>
    /// <param name="zs">all z values</param>
    public void SplitAxes(out List<float> xs, out List<float> ys, out List<float> zs)
    {
        xs = new List<float>(recordedPositions.Count);
        ys = new List<float>(recordedPositions.Count);
        zs = new List<float>(recordedPositions.Count);

        for (int i = 0; i < recordedPositions.Count; i++)
        {
            xs.Add(recordedPositions[i].x);
            ys.Add(recordedPositions[i].y);
            zs.Add(recordedPositions[i].z);
        }
    }

    /// <summary>
    /// Divide all positions by the normalization value.
    /// This changes the data inplace.
    /// </summary>
    public void Normalize()
    {
        // Only normalize if normalization-factor is availible
        if (normalization > float.Epsilon)
        {
            for (int i = 0; i < recordedPositions.Count; i++)
            {
                recordedPositions[i] /= normalization;
            }
        }
    }

    /// <summary>
    /// Serialize the data to JSON
    /// </summary>
    /// <param name="pretty">pretty print the JSON</param>
    /// <returns>a string containing the JSON</returns>
    public string ToJson(bool pretty = false)
    {
        return JsonUtility.ToJson(this, pretty);
    }

    /// <summary>
    /// Serialize and save the data to a JSON file.
    /// If the target directory does not exists it will be created.
    /// </summary>
    /// <param name="filename">destination filename</param>
    /// <param name="pretty">pretty print JSON</param>
    public void ToJsonFile(string filename, bool pretty = false)
    {
        string dir = Path.GetDirectoryName(filename);
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        File.WriteAllText(filename, this.ToJson(pretty));
    }

    /// <summary>
    /// Deserialize the data from a JSON string.
    /// </summary>
    /// <param name="jsonInput">string to deserialize</param>
    /// <returns>the deserialized data</returns>
    public static RecordedData FromJson(string jsonInput)
    {
        return JsonUtility.FromJson<RecordedData>(jsonInput);
    }

    /// <summary>
    /// Deserialize the data from a JSON file.
    /// </summary>
    /// <param name="filename">source filename</param>
    /// <returns>the deserialized data</returns>
    public static RecordedData FromJsonFile(string filename)
    {
        return FromJson(File.ReadAllText(filename));
    }
}
