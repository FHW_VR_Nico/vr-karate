using System;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom inspector for the RecordingTracker MonoBehaviour.
/// </summary>
[CustomEditor(typeof(RecordingTracker))]
public class RecordingTrackerEditor : Editor
{
    private bool foldoutAllSettings = false;

    public override void OnInspectorGUI()
    {
        RecordingTracker mTrack = (RecordingTracker)target;

        // Recording Information Settings
        // Only allow editing while in play mode
        GUI.enabled = Application.isPlaying;
        GUILayout.Space(5);
        GUILayout.Label("Recording Data", EditorStyles.boldLabel);
        mTrack.person = EditorGUILayout.TextField("Person Name", mTrack.person);
        mTrack.hitNum = (uint) Math.Max(0, EditorGUILayout.IntField("Next Hit Num", (int)mTrack.hitNum));

        // Show Record Button to start tracking
        // Only allow editing while in play mode and in menu state
        GUI.enabled = Application.isPlaying && mTrack.CurrentState == RecordingTracker.State.Menu;
        GUILayout.Space(10);
        GUILayout.Label("Actions", EditorStyles.boldLabel);
        if (GUILayout.Button("Record"))
        {
            mTrack.StartTracking();
        }

        // Technique Selection
        // Only allow editing while in play mode
        GUI.enabled = Application.isPlaying;
        GUILayout.Space(10);
        GUILayout.Label("Select technique", EditorStyles.boldLabel);
        GUILayout.Label("Current technique: " + mTrack.technique.ToString());
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Prev"))
        {
            mTrack.technique = Previous(mTrack.technique);
            mTrack.hitNum = 0;
        }
        if (GUILayout.Button("Next"))
        {
            mTrack.technique = Next(mTrack.technique);
            mTrack.hitNum = 0;
        }
        GUILayout.EndHorizontal();

        // Show a toggle to display all settings
        GUI.enabled = true;
        GUILayout.Space(10);
        foldoutAllSettings = GUILayout.Toggle(foldoutAllSettings, "Show all settings");
        if (foldoutAllSettings)
        {
            GUILayout.Space(10);
            DrawDefaultInspector();
        }
        GUILayout.Space(10);
    }

    /// <summary>
    /// Get the next Technique for a given Technique.
    /// Allows for roll over.
    /// </summary>
    /// <param name="src">source technique</param>
    /// <returns>next technique</returns>
    public static KarateTechnique Next(KarateTechnique src)
    {
        KarateTechnique[] Arr = (KarateTechnique[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<KarateTechnique>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }

    /// <summary>
    /// Get the previous Technique for a given Technique.
    /// Allows for roll over.
    /// </summary>
    /// <param name="src">source technique</param>
    /// <returns>previous technique</returns>
    public static KarateTechnique Previous(KarateTechnique src)
    {
        KarateTechnique[] Arr = (KarateTechnique[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<KarateTechnique>(Arr, src) - 1;
        return (-1 == j) ? Arr[Arr.Length - 1] : Arr[j];
    }
}
