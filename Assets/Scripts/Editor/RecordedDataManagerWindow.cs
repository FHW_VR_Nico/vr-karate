using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// A custom editor window to work with RecordedSets.
/// </summary>
public class RecordedDataManagerWindow : EditorWindow
{
    private static readonly string[] ALGORITHM_NAMES = { "DTW", "FastDTW" };

    private List<string> recordedDataFiles = new List<string>();
    private string selectedFilename = "";
    private string compareFilename = "";
    private List<string> mergeGroupFilenames = new List<string>();
    private RecordedSet recordedSet;
    private string newFilename = "";
    private string mergeFilename = "";
    private int selectedAlgorithm = 0;
    private TechSequence techSequence;
    private int techIdx = 0;
    private int scoreResult;

    // DTW result variables
    private Vector2 dtwResultScrollPos;
    private Vector2 windowScrollPos;
    private string[] inputSources = new string[0];
    private float[] lastCompareResultQuat = new float[0];
    private float[] lastCompareResultVec = new float[0];
    private float[] lastCompareResultX = new float[0];
    private float[] lastCompareResultY = new float[0];
    private float[] lastCompareResultZ = new float[0];

    // DTW timings
    private float vec3Timing = 0.0f;
    private float quatTiming = 0.0f;
    private float xTiming = 0.0f;
    private float yTiming = 0.0f;
    private float zTiming = 0.0f;

    /// <summary>
    /// Opens the window.
    /// </summary>
    [MenuItem("Window/Recorded Data")]
    public static void ShowWindow()
    {
        GetWindow<RecordedDataManagerWindow>().Show();
    }

    private void OnEnable()
    {
        titleContent = new GUIContent("Recorded Data");
        minSize = new Vector2(400, 300);
        UpdateRecordedSetList();
    }

    private void OnGUI()
    {
        windowScrollPos = EditorGUILayout.BeginScrollView(windowScrollPos);

        if (GUILayout.Button("Update"))
        {
            UpdateRecordedSetList();
        }

        GUILayout.BeginHorizontal();
        GUIFileList();
        GUIFileFullDetail();
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// Show a list of all RecordedSet files.
    /// </summary>
    private void GUIFileList()
    {
        GUILayout.BeginVertical();
        Color defaultBgColor = GUI.backgroundColor;

        foreach (string file in recordedDataFiles)
        {
            if (mergeGroupFilenames.Contains(file))
            {
                if (file == selectedFilename)
                {
                    GUI.backgroundColor = Color.blue;
                } else
                {
                    GUI.backgroundColor = Color.cyan;
                }
            }
            else if (file == selectedFilename)
            {
                GUI.backgroundColor = Color.green;
            }
            else if (file == compareFilename)
            {
                GUI.backgroundColor = Color.yellow;
            }

            if (GUILayout.Button(Path.GetFileName(file)))
            {
                SelectFile(file);
            }

            GUI.backgroundColor = defaultBgColor;
        }

        GUILayout.EndVertical();
    }

    /// <summary>
    /// Shows the right pane of the window with file details and operations.
    /// </summary>
    private void GUIFileFullDetail()
    {
        GUILayout.BeginVertical();
        if (selectedFilename != "")
        {
            GUIFileOperations();
            GUILayout.Space(10);
            GUIFileMetadata();
            GUILayout.Space(10);
            GUIFileContentScoring();
            GUILayout.Space(10);
            GUIFileContentEvaluation();
            GUILayout.Space(10);
            GUIFileContentMerge();
        }
        else
        {
            GUILayout.Label("No file selected.");
        }
        GUILayout.EndVertical();
    }

    /// <summary>
    /// Show GUI for simple file options (Rename and Delete).
    /// </summary>
    private void GUIFileOperations()
    {
        GUILayout.Label("File operations", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();

        newFilename = GUILayout.TextField(newFilename).Trim();
        GUI.enabled = newFilename.Length > 3;
        if (GUILayout.Button("Rename"))
        {
            string newPath = Path.GetDirectoryName(selectedFilename) + "/" + newFilename + ".json";
            File.Move(selectedFilename, newPath);
            newFilename = "";
            selectedFilename = newPath.Replace("\\", "/");
            UpdateRecordedSetList();
        }
        GUI.enabled = true;

        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        Color defaultFgColor = GUI.color;
        GUI.color = Color.red;
        if (GUILayout.Button("Delete this file"))
        {
            bool result = EditorUtility.DisplayDialog("Delete recording", "Do you really want to delete this recording?", "Yes", "No");
            if (result)
            {
                File.Delete(selectedFilename);
                UpdateRecordedSetList();
            }
        }
        GUI.color = defaultFgColor;
    }

    /// <summary>
    /// Show GUI for file metadata.
    /// </summary>
    private void GUIFileMetadata()
    {
        GUILayout.Label("Metadata", EditorStyles.boldLabel);
        GUILayout.Label("Filename: " + Path.GetFileName(selectedFilename));
        GUILayout.Label("Date: " + recordedSet.Datetime);
        GUILayout.Label("Count: " + recordedSet.Count);
        GUILayout.Label("Time: " + recordedSet.Time.ToString("0.00"));
    }

    /// <summary>
    /// Show GUI to perform a score calculation on the current file.
    /// </summary>
    private void GUIFileContentScoring()
    {
        GUILayout.Label("Score", EditorStyles.boldLabel);
        techSequence = (TechSequence) EditorGUILayout.ObjectField(techSequence, typeof(TechSequence), false);
        if (techSequence == null)
        {
            GUI.enabled = false;
            EditorGUILayout.Popup("Technique", 0, new string[]{ "???" });
            techIdx = 0;
        } else
        {
            string[] techNames = new string[techSequence.techniques.Count];
            for (int i = 0; i < techSequence.techniques.Count; i++)
            {
                techNames[i] = techSequence.techniques[i].label;
            }
            techIdx = EditorGUILayout.Popup("Technique", techIdx, techNames);
        }

        if (GUILayout.Button("Score now!"))
        {
            ScoreSelected();
        }

        GUILayout.Label("Score Result: " + scoreResult + "/5");

        GUI.enabled = true;
    }

    /// <summary>
    /// Show GUI to perform a time series comparison on the current file.
    /// </summary>
    private void GUIFileContentEvaluation()
    {
        GUILayout.Label("DTW Compare", EditorStyles.boldLabel);

        if (GUILayout.Button("Select for compare"))
        {
            compareFilename = selectedFilename;
        }

        if (GUILayout.Button("Compare with selected"))
        {
            CompareWithSelected();
        }

        selectedAlgorithm = EditorGUILayout.Popup("Algorithm", selectedAlgorithm, ALGORITHM_NAMES);

        // Show results
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        dtwResultScrollPos = EditorGUILayout.BeginScrollView(dtwResultScrollPos, GUILayout.MinHeight(80), GUILayout.MaxHeight(1000));

        for (int i = 0; i < inputSources.Length; i++)
        {
            GUILayout.Space(5);
            GUILayout.Label("DTW Result for " + inputSources[i], EditorStyles.boldLabel);

            GUILayout.Label("Vector3: " + lastCompareResultVec[i].ToString("0.000"));
            GUILayout.Label("Quaternion: " + lastCompareResultQuat[i].ToString("0.00"));
            GUILayout.Label(
                "X: " + lastCompareResultX[i].ToString("0.000") +
                " / Y: " + lastCompareResultY[i].ToString("0.000") +
                " / Z: " + lastCompareResultZ[i].ToString("0.000")
            );
        }

        // Show timings
        GUILayout.Space(5);
        GUILayout.Label("DTW Timings", EditorStyles.boldLabel);

        GUILayout.Label("Vector3: " + vec3Timing.ToString("0.00") + "ms");
        GUILayout.Label("Quaternion: " + quatTiming.ToString("0.00") + "ms");
        GUILayout.Label(
            "X: " + xTiming.ToString("0.00") +
            "ms / Y: " + yTiming.ToString("0.00") +
            "ms / Z: " + zTiming.ToString("0.00") + "ms"
        );

        EditorGUILayout.EndScrollView();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }

    /// <summary>
    /// Show GUI for merging multipe RecordedSets into a single one.
    /// </summary>
    private void GUIFileContentMerge()
    {
        GUILayout.Label("Content Merge", EditorStyles.boldLabel);

        if (GUILayout.Button("Add to merge group"))
        {
            mergeGroupFilenames.Add(selectedFilename);
        }

        GUILayout.BeginHorizontal();

        mergeFilename = GUILayout.TextField(mergeFilename).Trim();
        GUI.enabled = mergeFilename.Length > 3;
        if (GUILayout.Button("Merge!"))
        {
            // Do the merging
            string newPath = Path.GetDirectoryName(selectedFilename) + "/" + mergeFilename + ".json";
            RecordedSet resultSet = MergeFiles();
            resultSet.ToJsonFile(newPath);
            mergeFilename = "";
            selectedFilename = newPath.Replace("\\", "/");
            UpdateRecordedSetList();
            mergeGroupFilenames.Clear();
        }
        GUI.enabled = true;

        GUILayout.EndHorizontal();

        Color defaultFgColor = GUI.color;
        GUI.color = Color.red;
        if (GUILayout.Button("Clear merge Group"))
        {
            mergeGroupFilenames.Clear();
        }
        GUI.color = defaultFgColor;
    }

    /// <summary>
    /// Updates the list of RecordedSet files in the "recordings" directory.
    /// </summary>
    private void UpdateRecordedSetList()
    {
        recordedDataFiles.Clear();

        string basePath = Application.persistentDataPath + "/recordings";
        if (!Directory.Exists(basePath))
        {
            return;
        }

        foreach (string file in Directory.EnumerateFiles(basePath))
        {
            if (Path.GetExtension(file) != ".json")
            {
                continue;
            }

            recordedDataFiles.Add(file.Replace("\\", "/"));
        }
    }

    /// <summary>
    /// Change the currently selected file and load the RecordedSet into memory.
    /// </summary>
    /// <param name="filename">file to load</param>
    private void SelectFile(string filename)
    {
        selectedFilename = filename;
        recordedSet = RecordedSet.FromJsonFile(filename);
        recordedSet.Normalize();
    }

    /// <summary>
    /// Return the selected float comparision algorithm.
    /// </summary>
    /// <returns>selected algorithm</returns>
    private ITimeSeriesComparator<float> GetFloatAlgorithm()
    {
        switch (selectedAlgorithm)
        {
            case 0:
                return new DTW<float>((a, b) => Math.Abs(a - b));
            case 1:
                return new FastDTW<float>((a, b) => Math.Abs(a - b), (a, b) => (a + b) / 2.0f, 5);
            default:
                Debug.LogError("Unkown algorithm");
                return null;
        }
    }

    /// <summary>
    /// Return the selected vector comparision algorithm.
    /// </summary>
    /// <returns>selected algorithm</returns>
    private ITimeSeriesComparator<Vector3> GetVector3Algorithm()
    {
        switch (selectedAlgorithm)
        {
            case 0:
                return new DTW<Vector3>(Vector3.Distance);
            case 1:
                return new FastDTW<Vector3>(Vector3.Distance, (a, b) => (a + b) / 2.0f, 5);
            default:
                Debug.LogError("Unkown algorithm");
                return null;
        }
    }

    /// <summary>
    /// Return the selected quaternion comparision algorithm.
    /// </summary>
    /// <returns>selected algorithm</returns>
    private ITimeSeriesComparator<Quaternion> GetQuaternionAlgorithm()
    {
        switch (selectedAlgorithm)
        {
            case 0:
                return new DTW<Quaternion>(Quaternion.Angle);
            case 1:
                return new FastDTW<Quaternion>(Quaternion.Angle, (a, b) => Quaternion.Lerp(a, b, 0.5f), 5);
            default:
                Debug.LogError("Unkown algorithm");
                return null;
        }
    }

    /// <summary>
    /// Score the selected RecordedSet with the selected TechSequence and Technique.
    /// </summary>
    private void ScoreSelected()
    {
        ScoreCalculator scoreCalculator = new ScoreCalculator(
            techSequence.globalWeights,
            new DTW<Vector3>(Vector3.Distance),
            new DTW<Quaternion>(Quaternion.Angle)
        );
        scoreResult = scoreCalculator.Calculate(techSequence.techniques[techIdx].weights, techSequence.techniques[techIdx].recordedSet, recordedSet);
    }

    /// <summary>
    /// Perform time series comparision with the selected files and algorithm.
    /// </summary>
    private void CompareWithSelected()
    {
        RecordedSet compareSet = RecordedSet.FromJsonFile(compareFilename);
        compareSet.Normalize();

        ITimeSeriesComparator<float> algorithm = GetFloatAlgorithm();
        ITimeSeriesComparator<Vector3> algorithmVec3 = GetVector3Algorithm();
        ITimeSeriesComparator<Quaternion> algorithmQuat = GetQuaternionAlgorithm();

        float dataCount = (float)(recordedSet.Count);
        //Measure execution time of each algorithm
        float startTime;

        inputSources = recordedSet.InputSources;
        lastCompareResultX = new float[inputSources.Length];
        lastCompareResultY = new float[inputSources.Length];
        lastCompareResultZ = new float[inputSources.Length];
        lastCompareResultVec = new float[inputSources.Length];
        lastCompareResultQuat = new float[inputSources.Length];

        xTiming = 0.0f;
        yTiming = 0.0f;
        zTiming = 0.0f;
        vec3Timing = 0.0f;
        quatTiming = 0.0f;

        // Compare each input source of both time series
        for (int i = 0; i < inputSources.Length; i++)
        {
            RecordedData recordedData; 
            RecordedData compareData;
            recordedSet.GetData(inputSources[i], out recordedData);
            if (!compareSet.GetData(inputSources[i], out compareData))
            {
                inputSources[i] += " missing!";
                continue;
            }

            List<float> cmpXs, cmpYs, cmpZs, recXs, recYs, recZs;
            compareData.SplitAxes(out cmpXs, out cmpYs, out cmpZs);
            recordedData.SplitAxes(out recXs, out recYs, out recZs);

            // Compare axes
            startTime = Time.realtimeSinceStartup;
            lastCompareResultX[i] = algorithm.CalcDistance(cmpXs, recXs);
            lastCompareResultX[i] /= dataCount;
            xTiming += (Time.realtimeSinceStartup - startTime) * 1000.0f;

            startTime = Time.realtimeSinceStartup;
            lastCompareResultY[i] = algorithm.CalcDistance(cmpYs, recYs);
            lastCompareResultY[i] /= dataCount;
            yTiming += (Time.realtimeSinceStartup - startTime) * 1000.0f;

            startTime = Time.realtimeSinceStartup;
            lastCompareResultZ[i] = algorithm.CalcDistance(cmpZs, recZs);
            lastCompareResultZ[i] /= dataCount;
            zTiming += (Time.realtimeSinceStartup - startTime) * 1000.0f;

            // Compare positions
            startTime = Time.realtimeSinceStartup;
            lastCompareResultVec[i] = algorithmVec3.CalcDistance(compareData.recordedPositions, recordedData.recordedPositions);
            lastCompareResultVec[i] /= dataCount;
            vec3Timing += (Time.realtimeSinceStartup - startTime) * 1000.0f;

            // Compare orientations
            startTime = Time.realtimeSinceStartup;
            lastCompareResultQuat[i] = algorithmQuat.CalcDistance(compareData.recordedOrientations, recordedData.recordedOrientations);
            lastCompareResultQuat[i] /= dataCount;
            quatTiming += (Time.realtimeSinceStartup - startTime) * 1000.0f;
        }

        // Normalize timings
        xTiming /= (float)inputSources.Length;
        yTiming /= (float)inputSources.Length;
        zTiming /= (float)inputSources.Length;
        vec3Timing /= (float)inputSources.Length;
        quatTiming /= (float)inputSources.Length;
    }

    /// <summary>
    /// Merge the selected files.
    /// </summary>
    /// <returns>the merged RecordedSet</returns>
    private RecordedSet MergeFiles()
    {
        List<RecordedSet> mergeSets = new List<RecordedSet>(mergeGroupFilenames.Count);
        foreach (string name in mergeGroupFilenames)
        {
            mergeSets.Add(RecordedSet.FromJsonFile(name));
        }

        RecordedSetMerger merger = new RecordedSetMerger(mergeSets);
        return merger.MergeSets();
    }
}
