using UnityEngine;
using UnityEditor;
using System.IO;

/// <summary>
/// Shows a custom inspector for TechSequences.
/// </summary>
[CustomEditor(typeof(TechSequence))]
public class TechSequenceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.Space(20);

        if (GUILayout.Button("Import Recorded Set", GUILayout.Height(30)))
        {
            ImportRecordedSet();
        }

        GUILayout.Space(20);

        base.OnInspectorGUI();
    }

    /// <summary>
    /// Ask a user to import a RecordedSet JSON File into the current TechSequence and do so.
    /// </summary>
    private void ImportRecordedSet()
    {
        TechSequence seq = (TechSequence)target;

        string selectedFile = EditorUtility.OpenFilePanel("Select Recorded Set", Application.persistentDataPath + "/recordings", "json");
        if (selectedFile == "")
        {
            return;
        }

        RecordedSet recSet = RecordedSet.FromJsonFile(selectedFile);
        if (recSet == null)
        {
            Debug.LogWarning("Could not load recorded set!");
            return;
        }

        TechSequence.Entry newEntry = new TechSequence.Entry();
        newEntry.label = Path.GetFileNameWithoutExtension(selectedFile);
        newEntry.recordedSet = recSet;
        newEntry.technique = KarateTechnique.TrackerTest;

        seq.techniques.Add(newEntry);
        EditorUtility.SetDirty(target);
    }
}
