using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Valve.VR;

/// <summary>
/// This controller manages the full tracking process.
/// </summary>
[Serializable]
public class TrackingController
{
    private const int DEFAULT_CAPACITY = 300;

    public float trackingDelay = 3;
    public float trackingDuration = 2;

    [HideInInspector]
    public float armLength;

    /// <summary>
    /// Property to access the last recorded set.
    /// </summary>
    public RecordedSet LastRecordedSet { get; private set; }

    private float trackingCounter;

    private List<Transform> trackedObjs;
    private TextMeshProUGUI countdownLabel;

    private float startTime  = 0.0f;
    private bool delayMode   = false;
    private List<List<Vector3>> recordedPositions       = new List<List<Vector3>>(5);
    private List<List<Quaternion>> recordedOrientations = new List<List<Quaternion>>(5);
    private List<float> timings                         = new List<float>(DEFAULT_CAPACITY);

    private List<Vector3> startPos                      = new List<Vector3>(5);
    private List<Quaternion> startOrient                = new List<Quaternion>(5);

    /// <summary>
    /// Initialize the tracking controller.
    /// </summary>
    /// <param name="trackedObjs">list of all tracked objects</param>
    /// <param name="countdownLabel">label to show a countdown on</param>
    public void Init(List<Transform> trackedObjs, TextMeshProUGUI countdownLabel)
    {
        armLength = 0.0f;
        this.trackedObjs = trackedObjs;
        this.countdownLabel = countdownLabel;

        // Reserve required storage
        for (int i = 0; i < trackedObjs.Count; i++)
        {
            recordedPositions.Add(new List<Vector3>(DEFAULT_CAPACITY));
            recordedOrientations.Add(new List<Quaternion>(DEFAULT_CAPACITY));

            startPos.Add(new Vector3());
            startOrient.Add(new Quaternion());
        }
    }

    /// <summary>
    /// Start the tracking
    /// </summary>
    public void StartTracking()
    {
        delayMode = true;
        trackingCounter = trackingDelay;
        countdownLabel.enabled = true;
        Debug.Log("Start tracking delay...");
    }

    /// <summary>
    /// Call this every frame after StartTracking was called.
    /// Do not call this after tracking is done.
    /// </summary>
    /// <returns>true if the tracking is done, false if it is still running</returns>
    public bool Update()
    {
        if (delayMode)
        {
            // Wait a moment so the player is ready

            trackingCounter -= Time.deltaTime;
            countdownLabel.text = Math.Ceiling(trackingCounter).ToString("0");
            if (trackingCounter <= 0.0f)
                Tracking();
        }
        else
        {
            // Do the actual recording

            timings.Add(Time.time - startTime);

            for (int i = 0; i < trackedObjs.Count; i++)
            {
                // Subtract origin as a normalisation
                Vector3 pos = trackedObjs[i].position - startPos[i];
                Quaternion rot = trackedObjs[i].rotation * Quaternion.Inverse(startOrient[i]); 

                recordedPositions[i].Add(pos);
                recordedOrientations[i].Add(rot);
            }

            trackingCounter -= Time.deltaTime;
            if (trackingCounter <= 0.0f)
            {
                EndTracking();
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Prepare the real tracking by saving the origin and disabling the countdown.
    /// </summary>
    private void Tracking()
    {
        if (!delayMode)
        {
            Debug.LogError("Tried tracking while not in delay mode!");
            return;
        }

        Debug.Log("Start tracking...");
        countdownLabel.enabled = false;
        startTime = Time.time;
        delayMode = false;
        trackingCounter = trackingDuration;
        for (int i = 0; i < trackedObjs.Count; i++)
        {
            startPos[i] = trackedObjs[i].position;
            startOrient[i] = trackedObjs[i].rotation;
        }
    }

    /// <summary>
    /// Stop the tracking and create a RecordedSet out of the recorded movements.
    /// The RecordedSet is provided to the user of this class by the LastRecordedSet
    /// property.
    /// </summary>
    private void EndTracking()
    {
        if (delayMode)
        {
            Debug.LogError("Tried to end tracking while in delay mode!");
            return;
        }

        Debug.Log("Tracking done...");

        // Sanity check
        if (armLength < float.Epsilon)
        {
            Debug.LogError("ArmLength not set!");
        }

        RecordedSet recordedSet = new RecordedSet(timings, Time.time - startTime);
        for (int i = 0; i < trackedObjs.Count; i++)
        {
            // Search for the name of the input source
            string inputSource = "Unkown";
            SteamVR_Behaviour_Pose vrPose = trackedObjs[i].GetComponentInParent<SteamVR_Behaviour_Pose>();
            if (vrPose != null)
            {
                inputSource = vrPose.inputSource.ToString();
            }
            else if (trackedObjs[i].GetComponentInParent<SteamVR_CameraHelper>() != null)
            {
                inputSource = "Head";
            }

            // Create RecordedData and add it to the RecordedSet
            RecordedData data = new RecordedData(inputSource.ToString(), recordedPositions[i], recordedOrientations[i], startPos[i], startOrient[i], armLength);
            recordedSet.AddData(data);

            // Clear list for the next run
            recordedPositions[i].Clear();
            recordedOrientations[i].Clear();
        }

        timings.Clear();

        this.LastRecordedSet = recordedSet;
    }
}
