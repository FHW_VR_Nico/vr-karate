using System;
using UnityEngine;
using TMPro;

/// <summary>
/// This controller manages the user calibration.
/// </summary>
[Serializable]
public class CalibrationController
{
    private const string LABEL_ARM_DOWN = "Hold your arms down";
    private const string LABEL_ARM_UP = "Now hold your arms up";

    public Transform targetDummy;
    public float targetDummyHeight;

    public Transform playerHead;
    public float dummyOffset;
    public float calibrationDelay;

    public float ArmLength { get; private set; }

    private float calibrationCounter;
    private Vector3 armPos = Vector3.zero;
    private bool firstStep = false;

    private GameObject textCanvas;
    private TextMeshProUGUI textCanvasLabel;
    private TextMeshProUGUI countdownLabel;
    private Transform calibrationReference;

    /// <summary>
    /// Initialize the controller
    /// </summary>
    /// <param name="calibrationReference">the player hand transform</param>
    /// <param name="countdownLabel">a label to show a countdown</param>
    /// <param name="textCanvas">a canvas with a label to show instructions</param>
    public void Init(Transform calibrationReference, TextMeshProUGUI countdownLabel, GameObject textCanvas)
    {
        this.calibrationReference = calibrationReference;
        this.countdownLabel = countdownLabel;
        this.textCanvas = textCanvas;
        textCanvasLabel = textCanvas.GetComponentInChildren<TextMeshProUGUI>();
        ArmLength = 0.5f;
    }

    /// <summary>
    /// Start the calibration process
    /// </summary>
    public void StartCalibrate()
    {
        Debug.Log("Start calibration");
        firstStep = true;
        calibrationCounter = calibrationDelay;
        countdownLabel.enabled = true;
        textCanvas.SetActive(true);
        textCanvasLabel.text = LABEL_ARM_DOWN;
    }

    /// <summary>
    /// Call this every frame after StartCalibrate was called.
    /// Do not call this after calibration is done.
    /// </summary>
    /// <returns>true if the calibration is done, false if it is still running</returns>
    public bool Update()
    {
        calibrationCounter -= Time.deltaTime;
        countdownLabel.text = Math.Ceiling(calibrationCounter).ToString("0");

        // Only do the calibration after a short countdown to give the user a moment
        // to read, understand and follow the calibration instructions.
        if (calibrationCounter <= 0.0f)
        {
            return Calibrate();
        }

        return false;
    }

    /// <summary>
    /// Perform the calibration by reading the given transforms and maybe
    /// adjusting the dummy.
    /// This method is called twice in on calibration cycle.
    /// </summary>
    /// <returns>true if the calibration is done, false if it is still running</returns>
    private bool Calibrate()
    {
        if (firstStep)
        {
            armPos = calibrationReference.position;
            firstStep = false;
            calibrationCounter = calibrationDelay;
            textCanvasLabel.text = LABEL_ARM_UP;

            Debug.Log("Calibration: step 1 finished");
            return false;
        }
        else
        {
            ArmLength = (armPos - calibrationReference.position).magnitude / 2.0f;
            Debug.Log("Arm Length: " + ArmLength);

            // Adjust dummy size
            float eyeHeight = playerHead.transform.position.y;
            float dummyScale = eyeHeight / targetDummyHeight;
            targetDummy.localScale = new Vector3(dummyScale, dummyScale, dummyScale);

            // Adjust dummy position
            Vector3 oldPos = targetDummy.transform.localPosition;
            oldPos.z = Math.Clamp(ArmLength, 0.5f, 1.5f) + dummyOffset;
            targetDummy.transform.localPosition = oldPos;

            // Disable calibration UI
            countdownLabel.enabled = false;
            textCanvas.SetActive(false);
            Debug.Log("Calibration: finished");
            return true;
        }
    }
}
