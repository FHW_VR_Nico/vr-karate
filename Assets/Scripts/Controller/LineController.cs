using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This controller is responsible for drawing lines of a given list of vectors.
/// </summary>
[Serializable]
public class LineController
{
    private const float HUE_DELTA = 0.4357f;

    public GameObject prefabLineRenderer;
    private LineRenderer[] lineRenderers;

    /// <summary>
    /// Initialize the controller by creating the required line renderers.
    /// This also sets a unique color for all lines.
    /// </summary>
    /// <param name="parent">parent object in the scene for the line renders</param>
    /// <param name="count">how many line renderers to create</param>
    public void Init(Transform parent, int count)
    {
        lineRenderers = new LineRenderer[count];
        float hue = 0.0f;

        for (int i = 0; i < count; i++)
        {
            GameObject newLineRenderer = UnityEngine.Object.Instantiate(prefabLineRenderer);
            newLineRenderer.transform.SetParent(parent);
            lineRenderers[i] = newLineRenderer.GetComponent<LineRenderer>();
            lineRenderers[i].startColor = Color.HSVToRGB(hue, 1.0f, 1.0f);
            lineRenderers[i].enabled = false;

            hue += HUE_DELTA;
            if (hue > 1.0f)
            {
                hue -= 1.0f;
            }
        }
    }

    /// <summary>
    /// Draw a single line with the given points.
    /// </summary>
    /// <param name="points">the points to show</param>
    /// <param name="lineIdx">the line to draw, must be between 0 and the count from Init</param>
    public void DrawPointLine(List<Vector3> points, int lineIdx)
    {
        lineRenderers[lineIdx].positionCount = points.Count;

        for (int i = 0; i < points.Count; i++)
        {
            lineRenderers[lineIdx].SetPosition(i, points[i]);
        }

        lineRenderers[lineIdx].enabled = true;
    }

    /// <summary>
    /// Remove/disable all line renderers
    /// </summary>
    public void ClearPointLines()
    {
        foreach (LineRenderer lr in lineRenderers)
        {
            lr.enabled = false;
        }
    }
}
